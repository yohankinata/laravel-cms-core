<?php

use Faker\Factory;
use Illuminate\Database\Seeder;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('config')->insert([
            'title' => 'Default CMS Core Kincat',
            'logo' => 'config/logo_front.png',
            'logoSecond' => 'config/logo_back.png',
            'icon' => 'config/icon.png',
            'email' => 'kincat@gmail.com',
            'phone' => '08912312',
            'address' => 'Jalan Kincat Raya',
            'facebook' => 'http://www.facebook.com',
            'instagram' => 'http://www.instagram.com',
            'twitter' => 'http://www.twitter.com',
            'whatsapp' => '62898012312',
            'lat' => '-6.385589',
            'lng' => '106.830711',
        ]);
    }
}
