<?php

use Faker\Factory;
use Illuminate\Database\Seeder;

class AbsentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $absents = array();
        $type = ['PRETEND','ABSENT','SICK','PERMIT'];

        for ($i=1; $i <= 300; $i++){
            $data['student_id'] = mt_rand(1,50);
            $data['type'] = $type[mt_rand(0,3)];
            $data['remark'] = $faker->sentence($nbWords = 10, $variableNbWords = true);
            $data['record_date'] = $faker->dateTime();
            $data['created_at'] = $faker->dateTime();
            $data['updated_at'] = $faker->dateTime();
            array_push($absents, $data);
        }

        DB::table('absents')->insert($absents);
    }
}
