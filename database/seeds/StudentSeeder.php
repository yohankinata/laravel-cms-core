<?php

use Illuminate\Database\Seeder;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $students = array();

        for ($i=3; $i <= 53; $i++){
            $data['user_id'] = $i;
            $data['name'] = $faker->name;
            $data['grade'] = mt_rand(1,6);
            $data['birth_place'] = $faker->city;
            $data['birth_day'] = $faker->date();
            $data['address'] = $faker->address;
            $data['phone'] = $faker->phoneNumber;
            $data['join_date'] = $faker->date();
            $data['created_at'] = $faker->dateTime();
            $data['updated_at'] = $faker->dateTime();
            array_push($students, $data);
        }

        DB::table('students')->insert($students);
    }
}
