<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Absent extends Model
{
    protected $table = 'absents';
    protected $fillable = [
        'student_id',
        'type',
        'record_date',
        'remarks',
    ];

    const FORM_FIELD = [
        'student_id' => 'text',
        'type' => 'select',
        'record_date' => 'date',
        'remarks' => 'text',
    ];

    const FORM_DISABLED = [];

    const FORM_LABEL = [
        'student_id' => 'Id Siswa',
        'type' => 'Jenis Kehadiran',
        'record_date' => 'Tanggal Kehadiran',
        'remarks' => 'Keterangan',
    ];

    const FORM_HELP_LIST = [];

    const FORM_LABEL_HELP = [
    ];

    public function newQuery($excludeDeleted = true) {
        return parent::newQuery($excludeDeleted)
            ->where('absents.deleted', 0);
    }
}
