<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class SelectHelperProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        foreach(glob(app_path().'/Helpers/*.php') as $fileHelper){
            require_once($fileHelper);
        }
    }
}
