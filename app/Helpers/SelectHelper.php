<?php
namespace App\Helpers;

use App\User;
use App\Util\Constant;

class SelectHelper {
    /**
     * @param int $user_id User-id
     *
     * @return string
     */
    public static function getUserList() {
        $users = User::where('role','!=', Constant::USER_ROLE_ADMIN)->get();
        $data = [];

        foreach ($users as $user){
            $data[$user->id] = $user->name;
        }

        return $data;
    }

    public static function getGradeList() {
        $data = [];

        for ($i=1;$i<=6;$i++){
            $data[$i] = 'Kelas - '.$i;
        }

        return $data;
    }

    public static function getSelectList($type){
        if($type == 'getUserList'){
            return self::getUserList();
        }else if($type == 'getGradeList'){
            return self::getGradeList();
        }
    }
}
