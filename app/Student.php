<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'students';
    protected $fillable = [
        'user_id',
        'name',
        'grade',
        'birth_place',
        'birth_day',
        'address',
        'phone',
        'join_date',
    ];

    const FORM_FIELD = [
        'user_id' => 'select',
        'name' => 'text',
        'grade' => 'select',
        'birth_place' => 'text',
        'birth_day' => 'date',
        'address' => 'textarea',
        'phone' => 'text',
        'join_date' => 'date',
    ];

    const FORM_DISABLED = [];

    const FORM_LABEL = [
        'user_id' => 'User Id',
        'name' => 'Nama Lengkap',
        'grade' => 'Kelas',
        'birth_place' => 'Tempat Lahir',
        'birth_day' => 'Tanggal Lahir',
        'address' => 'Alamat',
        'phone' => 'Telepon',
        'join_date' => 'Tanggal Bergabung',
    ];

    const FORM_HELP_LIST = ['user_id'];

    const FORM_LABEL_HELP = [
        'user_id' => 'Relasi dari pengguna',
    ];

    const FORM_SELECT_LIST = [
        'user_id' => 'getUserList',
        'grade' => 'getGradeList',
    ];

    const FORM_VALIDATION = [
        'user_id' => 'required',
        'name' => 'required',
        'grade' => 'required',
        'birth_place' => 'required',
        'birth_day' => 'required',
        'join_date' => 'required',
    ];

    public function newQuery($excludeDeleted = true) {
        return parent::newQuery($excludeDeleted)
            ->where('students.deleted', 0);
    }
}
