@extends('frontend.layouts.template')

@section('pageTitle','Kirim Password')

@push('customCss')
    <style>
        .error-auth{
            text-align: center;
            padding: 20px;
            background-color: #ffdada;
            color: #7c7c7c
        }
    </style>
@endpush

@section('content')
    <!-- breadcrumb -->
    <div class="container">
        <div class="bread-crumb flex-w p-l-25 p-r-15 p-t-30 p-lr-0-lg">
            <a href="{{route('home')}}" class="stext-109 cl8 hov-cl1 trans-04">
                Beranda
                <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
            </a>

            <a href="{{route('login')}}" class="stext-109 cl8 hov-cl1 trans-04">
                Login
                <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
            </a>

            <span class="stext-109 cl4">
				Forgot
			</span>
        </div>
    </div>

    <!-- Forgot Section -->
    <form action="{{ route('password.email') }}" id="formConfirm" method="post" enctype="multipart/form-data" class="bg0 p-t-75 p-b-85">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-xl-7 m-lr-auto m-b-50">
                    <div class="m-l-25 m-r--38 m-lr-0-xl">
                        <div class="bor10 p-lr-40 p-t-30 p-b-40 m-l-63 m-r-40 m-lr-0-xl p-lr-15-sm">
                            <h4 class="mtext-109 cl2 p-b-30">
                                Kirim Password
                            </h4>

                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif

                            @if ($errors->has('username') || $errors->has('password'))
                                <div class="bor8 bg0 m-b-12 error-auth">
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                </div>
                            @endif

                            {{csrf_field()}}
                            <div class="bor8 bg0 m-b-12">
                                <input id="username" type="text" class="stext-111 cl8 plh3 size-111 p-lr-15" placeholder="Username" name="username" value="{{ old('username') }}" required autofocus>
                            </div>
                        </div>

                        <div class="flex-w flex-sb-m p-t-18 p-b-15 p-lr-40 p-lr-15-sm" style="float: right">
                            <button type="submit" class="flex-c-m stext-101 cl2 size-119 bg8 bor13 hov-btn3 p-lr-15 trans-04 pointer m-tb-10">
                                Kirim
                            </button>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-lg-3 p-b-80">
                    <div class="side-menu">
                        <div class="bor17 of-hidden pos-relative">
                            <form action="{{route('product')}}" method="get">
                                <input class="stext-103 cl2 plh4 size-116 p-l-28 p-r-55" type="text" name="keyword" placeholder="Cari Produk">

                                <button type="submit" class="flex-c-m size-122 ab-t-r fs-18 cl4 hov-cl1 trans-04">
                                    <i class="zmdi zmdi-search"></i>
                                </button>
                            </form>
                        </div>

                        <div class="p-t-55">
                            <h4 class="mtext-112 cl2 p-b-33">
                                Kategori
                            </h4>

                            <ul>
                                @foreach($resultCategories as $category)
                                    <li class="bor18">
                                        <a href="{{route('product').'?category='.$category->id}}" class="dis-block stext-115 cl6 hov-cl1 trans-04 p-tb-8 p-lr-4">
                                            {{ucwords($category->name.' ('.$category->total.')')}}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
